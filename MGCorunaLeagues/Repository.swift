//
//  RESTGateway.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class RESTGateway {
    
    func GET( url : NSURL, callback : (json : NSDictionary?, error : NSError?) -> () ) {
        
        let request = NSURLRequest(URL: url, cachePolicy: .ReloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            if error != nil {
                //Some error callback
                callback(json: nil, error: error)
            } else {
                if let json = try? NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? NSDictionary {
                    //Some callback
                    callback(json: json, error: nil)
                    return
                }
                callback(json: nil, error: NSError(domain: "com.RESTGateway.noJSON", code: 300, userInfo: [NSLocalizedDescriptionKey : "Response is not a JSON"]))
            }
        }
        task.resume()
        
        
    }
    
}
//
//  TableViewController.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class LeagueTableViewController : IndexedViewController {
    
    @IBOutlet private weak var tableView : UITableView?
    
    private var league : League!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.alpha = 0
    }
    
    func setLeague(league : League) {

        self.league = league
        title = league.name
    }
    
}

//MARK: Table View DataSource

extension LeagueTableViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = league.teams.count ?? 0
        if count > 0 && tableView.alpha == 0 {
            
            UIView.animateWithDuration(0.3, animations: {
                tableView.alpha = 1
            })
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(String(TeamTableViewCell)) as! TeamTableViewCell
        
        cell.configure(withTeam: league.teams[indexPath.row], leagueConditions: league.conditions, teamsInLeague: league.teams.count)
        
        return cell
    }
    
}

//MARK: Table View Delegate

extension LeagueTableViewController : UITableViewDelegate {
    
    
}
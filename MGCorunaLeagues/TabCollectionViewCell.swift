//
//  TabCollectionViewCell.swift
//  MGCorunaLeagues
//
//  Created by Mike on 03/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class TabCollectionViewCell : UICollectionViewCell {
    
    var title : UILabel = {
        
        $0.textColor = UIColor.whiteColor()
        $0.text = "Texto"
        $0.font = UIFont.boldSystemFontOfSize(17)
        $0.textAlignment = .Center
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
        
    }(UILabel())
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(title)
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[title]|", options: NSLayoutFormatOptions(rawValue : 0), metrics: nil, views: ["title" : title]))
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[title]|", options: NSLayoutFormatOptions(rawValue : 0), metrics: nil, views: ["title" : title]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

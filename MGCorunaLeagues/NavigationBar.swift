//
//  NavigationBar.swift
//  MGCorunaLeagues
//
//  Created by Mike on 03/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

//<color name="colorPrimary">#3F51B5</color>
//<color name="colorPrimaryLight">#596BCF</color>
//<color name="colorPrimaryDark">#303F9F</color>
//<color name="peter_river">#3498DB</color>
//<color name="colorAccent">#FF4081</color>
//
//<color name="emeraldGreen">#2ECC71</color>
//<color name="silver">#BDC3C7</color>
//<color name="asbestos">#7F8C8D</color>
//<color name="concrete">#95A5A6</color>
//<color name="black">#000</color>
//<color name="carrot">#E67E22</color>
//<color name="pomegranate">#C0392B</color>

import UIKit

protocol NavigationBarInterface {
    
    func pageChaged(toPage page : Int)
    func scrollToPoint(point : CGFloat)
    func reloadData()
}

protocol NavigationBarDelegate {
    
    func navBar(navBar : NavigationBar, didSelectTitle title : String, atIndex index : Int)
    
}

protocol NavigationBarDataSource {
    
    func navBarNumberOfItems(navBar : NavigationBar) -> Int
    func navBar(navBar : NavigationBar, titleForItemAtIndex index : Int) -> String
    
}

class NavigationBar : UIView {
    
    private var titles : UICollectionView!
    private var pageIndicator : UIPageControl!
    
    var navBarDelegate : NavigationBarDelegate?
    var navBarDataSource : NavigationBarDataSource?
    
    private struct Configuration {
        var pageIndicatorConfigured : Bool = false
    }
    
    private var config : Configuration = Configuration()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        style()
        initializeComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        style()
        initializeComponents()
    }

}

//MARK: Shared Initializer

extension NavigationBar {
    
    private func initializeComponents() {
    
        createTitleView()
        createPageIndicator()
    }
    
}

//MARK: Styling

extension NavigationBar {
    
    private func style() {
        //CUSTOM UI CHANGES
        
    }
    
}

//MARK: SubViews

extension NavigationBar {
    
    private func createTitleView() {
        
        let flow = CollectionViewHorizontalLayout()
        
        titles = UICollectionView(frame: CGRectZero, collectionViewLayout: flow)
//        titles.backgroundColor = UIColor(red: 43.0/100.0, green: 51.0/100.0, blue: 159.0/100.0, alpha: 1)
        titles.backgroundColor = .clearColor()
        titles.delegate = self
        titles.dataSource = self
        titles.translatesAutoresizingMaskIntoConstraints = false
        titles.registerClass(TabCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
//        titles.contentInset = UIEdgeInsetsMake(0, flow.itemSize.width, 0, 0)
        titles.userInteractionEnabled = false
        addSubview(titles)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titles]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["titles" : titles]))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[titles]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["titles" : titles]))
        
    }
    
    private func createPageIndicator() {
        
        pageIndicator = UIPageControl()
        pageIndicator.userInteractionEnabled = false
        pageIndicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(pageIndicator)
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[pageIndicator(10)]-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["pageIndicator" : pageIndicator]))
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[pageIndicator]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["pageIndicator" : pageIndicator]))
    }
}

//MARK: Navigation Bar Interface

extension NavigationBar : NavigationBarInterface {
    
    func pageChaged(toPage page: Int) {
        pageIndicator.currentPage = page
    }
    
    func scrollToPoint(point: CGFloat) {
        
        let components = modf(point)
        if components.1 == 0 {
            return
        }
        
        let itemWitdh = (titles.collectionViewLayout as! CollectionViewHorizontalLayout).itemSize.width
        titles.contentOffset.x = itemWitdh * point
    }
    
    func reloadData() {
        titles.reloadData()
    }
}

//MARK: CollectionView Delegate

extension NavigationBar : UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {

    }

}

//MARK: CollectionView DataSource

extension NavigationBar : UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let numberOfItems = navBarDataSource?.navBarNumberOfItems(self) ?? 0
        if !config.pageIndicatorConfigured && numberOfItems > 0 {
            pageIndicator.numberOfPages = numberOfItems
            config.pageIndicatorConfigured = true
            
            if titles.alpha == 0  || pageIndicator == 0 {
                
                UIView.animateWithDuration(0.3, animations: { 
                    
                    self.titles.alpha = 1
                    self.pageIndicator.alpha = 1
                })
            }
        }
        return numberOfItems
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! TabCollectionViewCell
        
        cell.title.text = navBarDataSource?.navBar(self, titleForItemAtIndex: indexPath.row)
        
        return cell
    }
    
}

//MARK: ScrollView Delegate

extension NavigationBar : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let page : Int = Int( Float(scrollView.contentOffset.x) / Float(CGRectGetWidth(scrollView.bounds)))
        
        navBarDelegate?.navBar(self, didSelectTitle: navBarDataSource?.navBar(self, titleForItemAtIndex: page) ?? "", atIndex: page)
    }
    
}
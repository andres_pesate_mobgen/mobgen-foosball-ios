//
//  LeagueRootViewController.swift
//  MGCorunaLeagues
//
//  Created by Mike on 03/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

protocol LeagueRootViewControllerInterface {
    
    func numberOfViews() -> Int
    func titleForView(atIndex index : Int) -> String
    func pagedChangedTo(page : Int)
    func setDelegate(delegate : LeagueRootViewControllerDelegate)
}

protocol LeagueRootViewControllerDelegate {
    
    func LeagueRootViewController(LeagueRootViewController : UIViewController, didChangeToPage page : Int)
    func LeagueRootViewController(LeagueRootViewController : UIViewController, didScrollToPoint point: CGFloat)
    func reloadData()
}

class LeagueRootViewController : UIViewController {
    
    @IBOutlet weak private var navBar: NavigationBar!
    @IBOutlet weak private var contentHeader: UIView!
    @IBOutlet weak private var activityIndicator: CircleActivityIndicator!
    @IBOutlet weak private var errorContainer: UIView!
    
    private var delegate: LeagueRootViewControllerDelegate?
    
    private var viewControllers = [IndexedViewController]()
    
    private lazy var pagedViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
    
    private var currentPage = 0
    
    private let useCase : GetLeaguesUseCaseInterface = LeagueDependencyFactory().getLeaguesUseCase()
    
    private var leagues : [League] = [League]()
    
    private var handler : NavigationBarHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handler = NavigationBarHandler.init(leagueRootViewController: self, navBar: navBar)
        
        loadContent()
        
        pagedViewController.dataSource = self
        pagedViewController.delegate = self
        pagedViewController.view.userInteractionEnabled = false
        
        for subView in pagedViewController.view.subviews {
            if subView is UIScrollView {
                (subView as! UIScrollView).delegate = self
            }
        }
        view.addSubview(pagedViewController.view)
        view.sendSubviewToBack(pagedViewController.view)
        
        let v = pagedViewController.view
        v.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[header][page]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["page": v, "header" : contentHeader]))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[page]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["page": v]))
    }
    
    @IBAction func reload(sender : UIButton) {
        loadContent()
    }
    
    private func loadContent() {
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.errorContainer.alpha = 0
            
            }, completion: { completed in
                
                self.errorContainer.hidden = true
        })
        
        activityIndicator.startAnimating()
        useCase.getLeagues { (leagues, error) in
            
            dispatch_async(dispatch_get_main_queue(), {
                
                self.activityIndicator.stopAnimating(true)
                if let _ = error {
                    
                    self.showErrorAlert()
                }
                else if let leagues = leagues {
                    
                    self.pagedViewController.view.hidden = false
                    self.pagedViewController.view.userInteractionEnabled = true
                    self.leagues = leagues
                    
                    self.presentChildren()
                }
            })
        }
    }
    
    private func presentChildren() {
        
        var tableViews = [IndexedViewController]()
        
        for (idx, league) in leagues.enumerate() {
            let vC = storyboard?.instantiateViewControllerWithIdentifier(String(LeagueTableViewController)) as! LeagueTableViewController
            vC.index = idx
            vC.setLeague(league)
            
            tableViews.append(vC)
        }
        
        viewControllers = tableViews
        pagedViewController.setViewControllers([viewControllers.first!], direction: .Forward, animated: true, completion: nil)
        currentPage = 0
        
        delegate?.reloadData()
    }
    
    private func showErrorAlert() {
        let alert = UIAlertController(title: "😓", message: "Something went wrong!", preferredStyle: .Alert)
        let tryAgain = UIAlertAction(title: "Try Again! 😁", style: .Cancel, handler: { (action) in
            
            self.loadContent()
        })
        let cancel = UIAlertAction(title: "Noup Bye! 😡", style: .Default, handler: { (action) in
            
            UIView.animateWithDuration(0.3,
                animations: {
                    
                    self.pagedViewController.view.hidden = true
                    self.errorContainer.hidden = false
                    self.errorContainer.alpha = 1
                }, completion: { completed in
                    
                    
            })
        })
        alert.addAction(tryAgain)
        alert.addAction(cancel)
        presentViewController(alert, animated: true, completion: nil)
    }
    
}

//MARK: LeagueRootViewController Interface

extension LeagueRootViewController : LeagueRootViewControllerInterface {
    
    func setDelegate(delegate: LeagueRootViewControllerDelegate) {
        self.delegate = delegate
    }
    
    func numberOfViews() -> Int {
        return viewControllers.count | 0
    }
    
    func titleForView(atIndex index : Int) -> String {
        return viewControllers[index].title ?? "League"
    }
    
    func pagedChangedTo(page: Int) {
        guard page >= 0 && page < viewControllers.count else {
            return
        }
        
        defer {
            currentPage = page
        }
        
        let direction : UIPageViewControllerNavigationDirection = page > currentPage ? .Forward : .Reverse
        pagedViewController.setViewControllers([viewControllers[page]], direction: direction, animated: true, completion: nil)
    }
    
}

//MARK: UIPagedViewController DataSource

extension LeagueRootViewController : UIPageViewControllerDataSource {
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController LeagueRootViewController: UIViewController) -> UIViewController? {
        
        let LeagueRootViewController = LeagueRootViewController as! IndexedViewController
        
        let page = LeagueRootViewController.index + 1
        
        if page >= viewControllers.count {
            return nil
        }
        
        currentPage = page
        return viewControllers[page]
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController LeagueRootViewController: UIViewController) -> UIViewController? {
        
        let LeagueRootViewController = LeagueRootViewController as! IndexedViewController
        
        let page = LeagueRootViewController.index - 1
        
        if page < 0 {
            return nil
        }
        
        currentPage = page
        return viewControllers[page]
    }
    
}

//MARK: UIPagedViewController Delegate

extension LeagueRootViewController : UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if completed {
            delegate?.LeagueRootViewController(self, didChangeToPage: (pageViewController.viewControllers?.last as! IndexedViewController).index)
        }
        
    }
    
}

//MARK : UIScrollView Delegate

extension LeagueRootViewController: UIScrollViewDelegate {
    
    static private var page : Int! = 0
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let point = (scrollView.contentOffset.x - view.frame.size.width)/view.frame.size.width + CGFloat(LeagueRootViewController.page)
        delegate?.LeagueRootViewController(self, didScrollToPoint: point)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        LeagueRootViewController.page = (pagedViewController.viewControllers?.last as! IndexedViewController).index
    }
    
}
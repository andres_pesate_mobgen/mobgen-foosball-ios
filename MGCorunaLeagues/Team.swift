//
//  Team.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class Team {
    
    private let kDrawGamesNode = "draw_games"
    private let kGoalsAgainstNode = "goals_against"
    private let kGoalsDifferenceNode = "goals_diference"
    private let kGoalsForNode = "goals_for"
    private let kLostGamesNode = "lost_games"
    private let kPlayersNode = "players"
    private let kPointsNode = "points"
    private let kPositionNode = "position"
    private let kWonGamesNode = "win_games"
    
    let drawGames : String
    let goalsAgainst : String
    let goalsDifference : String
    let goalsFor : String
    let lostGames : String
    let players : String
    let points : String
    let position : String
    let wonGames : String
    
    var gamesPlayed : String {
        
        return String(Int(drawGames)! + Int(lostGames)! + Int(wonGames)!)
    }
    
    var frontPlayer : String {
        
        return players.componentsSeparatedByString(" - ").first!
    }
    
    var backPlayer : String {
        
        return players.componentsSeparatedByString(" - ").last!
    }
    
    init?(withJSON json : NSDictionary) {
        
        guard let drawGames = json[kDrawGamesNode] as? String,
            goalsAgainst    = json[kGoalsAgainstNode] as? String,
            goalsDifference = json[kGoalsDifferenceNode] as? String,
            goalsFor        = json[kGoalsForNode] as? String,
            lostGames       = json[kLostGamesNode] as? String,
            players         = json[kPlayersNode] as? String,
            points          = json[kPointsNode] as? String,
            position        = json[kPositionNode] as? String,
            wonGames        = json[kWonGamesNode] as? String else {
                return nil
        }
        self.drawGames = drawGames
        self.goalsAgainst = goalsAgainst
        self.goalsDifference = goalsDifference
        self.goalsFor = goalsFor
        self.lostGames = lostGames
        self.players = players
        self.points = points
        self.position = position
        self.wonGames = wonGames
    }
}
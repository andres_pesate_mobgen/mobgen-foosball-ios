//
//  Colors.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    class func emeraldGreenColor() -> UIColor {
        return UIColor(netHex: 0x2ECC71)
    }
    
    class func peterRiverColor() -> UIColor {
        return UIColor(netHex: 0x3498DB)
    }
    
    class func pomegranateColor() -> UIColor {
        return UIColor(netHex: 0xC0392B)
    }
    
    class func silverColor() -> UIColor {
        return UIColor(netHex: 0xBDC3C7)
    }
}
//
//  GetLeaguesUseCase.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

protocol GetLeaguesUseCaseInterface {
    
    func getLeagues(completion : ([League]?, NSError?) -> () )
    
}

class GetLeaguesUseCase {

    private let repository : LeaguesRepositoryInterface
    
    init(repository : LeaguesRepositoryInterface) {
        
        self.repository = repository
    }

}

extension GetLeaguesUseCase : GetLeaguesUseCaseInterface {
    
    func getLeagues(completion: ([League]?, NSError?) -> ()) {
        repository.getLeagues(completion)
    }
    
}
//
//  TeamsBuilder.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class TeamsBuilder {
    
    func buildLeagues(fromJSON json: [NSDictionary]?) -> [Team]? {
        
        guard let json = json else {
            return nil
        }
        
        var teams = [Team]()
        for item in json {
            if let team = Team.init(withJSON: item) {
                teams.append(team)
            }
        }
        
        return teams
        
    }
    
}
//
//  LeagueDependencyFactory.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class LeagueDependencyFactory {
    
    func getLeaguesUseCase() -> GetLeaguesUseCaseInterface {
        return GetLeaguesUseCase(repository: self.leaguesRepository())
    }
    
    private func leaguesRepository() -> LeaguesRepositoryInterface {
        return LeaguesRepository(remoteService: self.leagueRemoteService())
    }
    
    private func leagueRemoteService() -> LeagueRemoteService {
        return LeaguesRESTAPI(gateway: RESTGateway())
    }
}
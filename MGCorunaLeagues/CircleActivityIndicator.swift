//
//  CircleActivityIndicator.swift
//  MGCorunaLeagues
//
//  Created by Mike on 08/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

@IBDesignable
class CircleActivityIndicator : UIView {
    
    @IBInspectable var color : UIColor = UIColor.orangeColor()
    
//    private var sonarLayer: CALayer!
    
    func startAnimating() {
        
        fadingAnimation()
        sonarAnimation()
        
        hidden = false
    }
    
    private func fadingAnimation() {

        let circle = CALayer()
        let dimensions = CGSizeMake(CGRectGetWidth(bounds), CGRectGetHeight(bounds))
        circle.bounds = CGRect(x: 0.0, y: 0.0, width: dimensions.width, height: dimensions.height)
        circle.position = CGPoint(x: dimensions.width / 2.0, y: dimensions.height / 2.0)
        circle.cornerRadius = 1
        
        circle.backgroundColor = UIColor.whiteColor().CGColor
        circle.cornerRadius = (max(dimensions.height, dimensions.width) as CGFloat) / 2
        circle.borderColor = color.CGColor
        circle.borderWidth = (max(dimensions.height, dimensions.width) as CGFloat) * 0.1
        
        layer.addSublayer(circle)
        
        let animation = CABasicAnimation(keyPath: "opacity")
        animation.repeatCount = Float(INTMAX_MAX)
        animation.autoreverses = true
        animation.fromValue = 1
        animation.toValue = 0.2
        animation.duration = 0.75
        circle.addAnimation(animation, forKey: "opacity")
    }
    
    private func sonarAnimation() {
        
        let dimensions = CGSizeMake(CGRectGetWidth(bounds), CGRectGetHeight(bounds))
        
        let r = CAReplicatorLayer()
        r.bounds = CGRect(x: 0.0, y: 0.0, width: dimensions.width*3, height: dimensions.height*3)
        r.position = CGPointMake(dimensions.width / 2.0, dimensions.height / 2.0)
        r.backgroundColor = UIColor.clearColor().CGColor
        layer.addSublayer(r)

        let circle = CALayer()
        circle.borderColor = UIColor.orangeColor().CGColor
        circle.bounds = CGRect(x: 0.0, y: 0.0, width: dimensions.width, height: dimensions.height)
        circle.position = CGPoint(x: dimensions.width*3 / 2.0, y: dimensions.height*3 / 2.0)
        circle.cornerRadius = max(dimensions.height, dimensions.width) / 2.0
        circle.backgroundColor = UIColor.orangeColor().colorWithAlphaComponent(0.5).CGColor
        
        r.addSublayer(circle)
        
        let scale = CABasicAnimation(keyPath: "transform.scale")
        scale.toValue = 7.0
        scale.duration = 1.5
        scale.repeatCount = Float.infinity
        circle.addAnimation(scale, forKey: nil)
        
        let opacity = CABasicAnimation(keyPath: "opacity")
        opacity.fromValue = 0.5
        opacity.toValue = 0
        opacity.duration = 1.5
        opacity.repeatCount = Float.infinity
        circle.addAnimation(opacity, forKey: nil)
        
        r.instanceCount = 3
        r.instanceDelay = 0.75
        
        let cover = CALayer()
        cover.borderColor = UIColor.whiteColor().CGColor
        cover.bounds = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        cover.position = CGPoint(x: dimensions.width / 2.0, y: dimensions.height / 2.0)
        cover.cornerRadius = 10
        cover.backgroundColor = UIColor.whiteColor().CGColor
        layer.addSublayer(cover)

        
    }
    
    func stopAnimating(hide : Bool) {
        
        layer.removeAllAnimations()

        for sublayer : CALayer in layer.sublayers! {
            
            sublayer.removeAllAnimations()
            sublayer.removeFromSuperlayer()
        }
        
        hidden = hide
    }
    
    override func drawRect(rect: CGRect) {
        
        super.drawRect(rect)
//        layer.backgroundColor = UIColor.whiteColor().CGColor
//        layer.cornerRadius = (max(CGRectGetWidth(rect), CGRectGetHeight(rect)) as CGFloat) / 2
//        layer.borderColor = color.CGColor
//        layer.borderWidth = (max(CGRectGetWidth(rect), CGRectGetHeight(rect)) as CGFloat) * 0.1
        
    }
}

//
//  CollectionViewHorizontalLayout.swift
//  MGCorunaLeagues
//
//  Created by Mike on 03/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class CollectionViewHorizontalLayout : UICollectionViewFlowLayout {
    
    override func prepareLayout() {
        super.prepareLayout()
        
        scrollDirection = .Horizontal
        
        minimumInteritemSpacing = 0;
        minimumLineSpacing = 0;
        itemSize = CGSizeMake(CGRectGetWidth(UIScreen.mainScreen().bounds), 44-8);
        sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        collectionView?.pagingEnabled = true
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator = false
    }
}

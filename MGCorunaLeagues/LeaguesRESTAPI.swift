//
//  LeaguesRESTAPI.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

protocol LeagueRemoteService {
    
    func getLeagues(completion : ([League]?, NSError?) -> () )
    
}

class LeaguesRESTAPI {
    
    private var gateway : RESTGateway
    
    private let URL : NSURL = NSURL(string: "https://luminous-torch-2242.firebaseio.com/kimono/api/94wdsmie/latest.json?auth=WU6EmqmAyBxGdZAYCDjUnRCGq2yzf0weU2BWy0Zi")!
    
    init(gateway : RESTGateway) {
        
        self.gateway = gateway
    }
    
}

//MARK: League Remote Service

extension LeaguesRESTAPI : LeagueRemoteService {
    
    func getLeagues(completion: ([League]?, NSError?) -> ()) {
    
        gateway.GET(URL) { (json, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            let builder = LeagueBuilder()
            if let leagues = builder.buildLeagues(fromJSON: json) {
                completion(leagues, nil)
                return
            }
            
            completion(nil, error)
        }
    }
    
}
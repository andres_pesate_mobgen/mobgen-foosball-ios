//
//  NavigationBarHandler.swift
//  MGCorunaLeagues
//
//  Created by Mike on 03/06/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class NavigationBarHandler : NSObject {
    
    private var control : LeagueRootViewControllerInterface!
    private var navBarInterface : NavigationBarInterface!
    
    init?(leagueRootViewController : LeagueRootViewControllerInterface, navBar : NavigationBar) {
        
        super.init()
        
        control = leagueRootViewController
        control.setDelegate(self)
        navBarInterface = navBar
        navBar.navBarDelegate = self
        navBar.navBarDataSource = self
    }
    
}

//MARK: NavBar Datasource

extension NavigationBarHandler : NavigationBarDataSource {
    
    func navBarNumberOfItems(navBar: NavigationBar) -> Int {
        return control.numberOfViews()
    }
    
    func navBar(navBar: NavigationBar, titleForItemAtIndex index: Int) -> String {
        return control.titleForView(atIndex: index)
    }
}

//MARK: NavBar delegate

extension NavigationBarHandler : NavigationBarDelegate {
    
    func navBar(navBar : NavigationBar, didSelectTitle title : String, atIndex index : Int) {
        
        control.pagedChangedTo(index)
        
    }
}

//MARK: LeagueRootViewController Delegate 

extension NavigationBarHandler : LeagueRootViewControllerDelegate {
    
    func LeagueRootViewController(LeagueRootViewController: UIViewController, didChangeToPage page: Int) {
        navBarInterface.pageChaged(toPage: page)
    }
    
    func LeagueRootViewController(LeagueRootViewController: UIViewController, didScrollToPoint point: CGFloat) {
        navBarInterface.scrollToPoint(point)
    }
    
    func reloadData() {
        navBarInterface.reloadData()
    }
}

//
//  LeaguesDataStore.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

protocol LeaguesRepositoryInterface {
    
    func getLeagues(completion : ([League]?, NSError?) -> () )
    
}

class LeaguesRepository {
    
    private var leagues : [League]!
    
    private var remoteService : LeagueRemoteService
    
    private let URL : NSURL = NSURL(string: "https://luminous-torch-2242.firebaseio.com/kimono/api/94wdsmie/latest.json?auth=WU6EmqmAyBxGdZAYCDjUnRCGq2yzf0weU2BWy0Zi")!
    
    init(remoteService : LeagueRemoteService) {
        
        self.remoteService = remoteService
    }
}

extension LeaguesRepository : LeaguesRepositoryInterface {
    
    func getLeagues(completion: ([League]?, NSError?) -> ()) {
        
        if let leagues = leagues {
            completion(leagues, nil)
            return
        }
        
        remoteService.getLeagues { (leagues, error) in

            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let leagues = leagues else {
                completion(nil, error)
                return
            }
            
            self.leagues = leagues
            completion(leagues, nil)
        }
        
    }
    
}
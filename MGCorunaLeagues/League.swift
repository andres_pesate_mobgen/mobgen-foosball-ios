//
//  League.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class League {
    
    struct Conditions {
        let greenZonePositions : Int
        let blueZonePositions : Int
        let redZonePosition : Int
    }

    private let kNameNode = "name"
    private let kLeaderboardNode = "leaderboard"
    
    let name : String!
    let teams : [Team]
    let conditions : Conditions
    
    init?(withJSON json : NSDictionary, conditions : Conditions?) {
        
        let teamsBuilder = TeamsBuilder()
        
        guard let name = json[kNameNode] as? String, teams = teamsBuilder.buildLeagues(fromJSON: json[kLeaderboardNode] as? [NSDictionary]), conditions = conditions else
        {
            return nil
        }
        
        self.teams = teams
        self.name = name
        self.conditions = conditions
    }
    
}